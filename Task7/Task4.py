#класс рациональных дробей

class Rational(object):
   

    def __init__(self, numerator, denominator):
        if (denominator == 0): raise ZeroDivisionError
        if (denominator == 0): raise Exception("Ноль не представим в виде дроби") #наверное

        if numerator == denominator:
            self.numerator = self.denominator = 1
        elif ((numerator > 0) and (denominator > 0)) or ((numerator < 0) and (denominator < 0)):
            self.denominator = denominator
            self.numerator = numerator
            self.reduce()
        elif (numerator < 0) or (denominator < 0):
            self.denominator = abs(denominator)
            self.numerator = abs(numerator)
            self.reduce()
            self.numerator *= -1

    def __add__(self, other):
        result = Rational(self.numerator*other.denominator + other.numerator*self.denominator, self.denominator*other.denominator)
        result.reduce()
        return result

    def __sub__(self, other):
        result = Rational(self.numerator*other.denominator - other.numerator*self.denominator, self.denominator*other.denominator)
        result.reduce()
        return result
        
    def __mul__(self, other):
        result = Rational(self.numerator*other.numerator, self.denominator*other.denominator)
        result.reduce()
        return result

    def __truediv__(self, other):
        result = Rational(self.numerator*other.denominator, self.numerator*other.denominator)
        result.reduce()
        return result

    def reduce(self):
        flag = False
        if self.numerator < 0:
            self.numerator = -self.numerator
            flag = True

        maxval = self.numerator if self.numerator > self.denominator else self.denominator
        for i in range(maxval,2,-1):
            if (self.numerator % maxval == 0) & (self.denominator % maxval == 0):
                self.numerator /=  maxval
                self.denominator /=  maxval
                break
        if flag: self.numerator *= -1