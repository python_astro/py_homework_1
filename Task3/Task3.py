#сортирует строки в файле

inpath = "Text.txt"
outpath = "Sorted"+inpath
lines = []

with open(inpath, 'r') as input:
    for line in input:
        lines.append(line)
    lines.sort()

with open(outpath, 'w') as output:
    for line in lines:
        output.write(line)
